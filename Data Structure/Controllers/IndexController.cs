﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Data_Structure.Controllers
{
    /* 
        Authors:        Michael Niemann, Taylor Bakow, Hunter Riches, Sam Gines
        Description:    This program tracks exactly how many hamburgers each customer in a queue purchases.  The customer's name 
                        as well as the amount of hamburgers they purchased is stored in a dictionary; if that customer comes 
                        back to purchase additional hamburgers, the number purchased will cumulatively increase.
    */
    public class IndexController : Controller
    {

        Queue<String> qsOrderQueue = new Queue<string>();                       //This queue holds the customers purchasing hamburgers in the order which they will purchase them
        Dictionary<String, int> dCustomerInfo = new Dictionary<string, int>();  //This dictionary will store customer's name along with the number of hamburgers they have purchased


        //The 'random' and 'randomName' functions are used to renerate a random name for the customers that will be added to the queue
        public static Random random = new Random();

        public static string randomName()
        {
            string[] names = new string[8] { "Sam Gines", "Hunter Riches", "Taylor Bakow", "Hingle McCringleberry", "Smitty Jensen", "Michael Neiiiiigghman", "Greg Anderson", "Spongebob Squarepants"};
            int randomIndex = Convert.ToInt32(random.NextDouble() * 7);
            return names[randomIndex];
        }

        //Generates an integer: random number of burgers purchased.  To be called for each customer, number between 0 and 20 (inclusive)
        public static int randomNumberInRange()
        {
            return Convert.ToInt32(random.NextDouble() * 20);
        }


        //'addCustomers' function will initialize a name variable, then in a loop will generate 100 randomized names and add them to a queue.  Some of those names will be the same.
        public void addCustomers()
        {
            string name = "";      
            for (int iCount = 0; iCount < 100; iCount++)
            {
                name = randomName();              
                qsOrderQueue.Enqueue(name);              
            }     
        }


        //The 'processCustomers' function will enumerate through the queue and check if each customer has been inputed in the dictionary already.  If they have not, they will be entered in along with the amount of hamburgers they purchased.  If they have, they will have the amount of hamburgers purchased added onto their running total. 
        public void processCustomers()
        {
            int numBurgers = 0;
            int i = 0;

            //IEnumerator<string> MyQueueEnumerator = qsOrderQueue.GetEnumerator();

            while (i < 100)
            {
                ++i;
                string custName = qsOrderQueue.Dequeue();
                numBurgers = randomNumberInRange();

                if (dCustomerInfo.ContainsKey(custName))
                {
                    dCustomerInfo[custName] += numBurgers;
                }
                else
                {
                    dCustomerInfo.Add(custName, numBurgers);
                }

               // qsOrderQueue.Dequeue();
            }
            
            
        }


        //'printCustomers' prints out the name of the customers in the dictionary alongside the total number of hamburgers that individual has purchased
        public void printCustomers()
        {
            string name = "";
            int burgersPurchased = 0;

            ViewBag.Output += "<table>";

            foreach (var item in dCustomerInfo)
            {
                name = item.Key;
                burgersPurchased = item.Value;
                ViewBag.Output += "<tr>";
                ViewBag.Output += "<td>" + name + "</td>";
                ViewBag.Output += "<td>" + burgersPurchased + "</td>";
                ViewBag.Output += "</tr>";
                
                    
            }
            ViewBag.Output += "</table>";



            //return View();
        }

         
        

        // GET: Index
        public ActionResult Index()
        {
            addCustomers();
            processCustomers();
            printCustomers();
            return View();
        }


    }
}